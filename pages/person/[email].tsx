import { useRouter } from 'next/router';
import React, { ChangeEvent, useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, message } from 'antd';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';

const PersonDetail = () => {
  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );
  const [isEditing, setIsEditing] = useState(false);

  const [name, setName] = useState('');
  const [gender, setGender] = useState('');
  const [phone, setPhone] = useState('');
  const [birthday, setBirthday] = useState('');

  useEffect(() => {
    load();
  }, []);

  console.log(data);

  useEffect(() => {
    setName(data.name);
    setGender(data.gender);
    setPhone(data.phone);
    setBirthday(data.birthday);
  }, [data]);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  const onInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { id, value } = event.target;

    switch (id) {
      case 'name-input':
        setName(value);
        break;
      case 'gender-input':
        setGender(value);
        break;
      case 'phone-input':
        setPhone(value);
        break;
      case 'birthday-input':
        setBirthday(value);
        break;
    }
  };

  const onSaveClicked = () => {
    save({
      ...data,
      name,
      gender,
      phone,
      birthday,
    });

    setIsEditing(false);
  };

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button
            type="default"
            onClick={() => {
              if (isEditing) {
                onSaveClicked();
              } else {
                setIsEditing(true);
              }
            }}
          >
            {isEditing ? 'Save' : 'Edit'}
          </Button>,
        ]}
      >
        {data && (
          <>
            {isEditing ? (
              <>
                <input
                  id="name-input"
                  placeholder="Name"
                  onChange={onInputChange}
                  value={name}
                />
                <input
                  id="gender-input"
                  placeholder="Gender"
                  onChange={onInputChange}
                  value={gender}
                />
                <input
                  id="phone-input"
                  placeholder="Phone"
                  onChange={onInputChange}
                  value={phone}
                />
                <input
                  id="birthday-input"
                  placeholder="Birthday"
                  onChange={onInputChange}
                  value={birthday}
                />
              </>
            ) : (
              <Descriptions size="small" column={1}>
                <Descriptions.Item label="Name">{data.name}</Descriptions.Item>
                <Descriptions.Item label="Gender">{data.gender}</Descriptions.Item>
                <Descriptions.Item label="Phone">{data.phone}</Descriptions.Item>
                <Descriptions.Item label="Birthday">{data.birthday}</Descriptions.Item>
              </Descriptions>
            )}
          </>
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
